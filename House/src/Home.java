public class Home {

	//The following public values are what we are describing in this programming
	public String walls;
	public String floor = "";
	public int windows;
	
	//The following three Strings are the options for the floor
	String floorWood = "Hardwood";
	String floorCarpet = "Carpeted";
	String floorTile = "Tiled";
	
	/**
	 * Initial values
	 */
	public Home()
	{
		walls = "No color yet.";
		floor = "No floor yet.";
		windows = 0;
	}
	
	/**
	 * Set a new value for walls		
	 * @param newWalls 
	 */
	public void setWalls(String newWalls)
	{
		walls = newWalls;
	}
	
	/**
	 * Sets a new value for floor
	 * @param newFloor
	 */
	public void setFloor(String newFloor)
	{
		//The following if-else statement forces the program to end if one of the three floor options are not used
		if ((newFloor.equals(floorWood)) || (newFloor.equals(floorCarpet)) || (newFloor.equals(floorTile)))
		{
			floor = newFloor;		}
		else
		{
			System.out.println("Error: Not a type of floor.");
			System.exit(0);

		}
	
	}
	
	/**
	 * Sets a new value for windows
	 * @param newWindows
	 */
		public void setWindows(int newWindows)
	{
		//The following if-the statement forces the program to end if a negative value is used.	
		if (newWindows < 0)
		{
			System.out.println("Error: Can't have a negative amount of windows.");
			System.exit(0);
		}
		else
		{
			windows = newWindows;
		}
	}
	
	/**
	 * 	This returns the wall value from setWalls
	 * @return wall color
	 */
	public String getWalls()
	{
		return walls;
	}
	
	/**
	 * This returns the floor value from setFloor
	 * @return type of floor
	 */
	public String getFloor()
	{
		return floor;
	}
	
	/**
	 * This returns the window value from setWindows
	 * @return number of windows
	 */
	public int getWindows()
	{
		return windows;
	}
	
	/**
	 * This tells the user what the room is made up of
	 */
	public void writeOutput()
	{
		System.out.println(walls + " walls,");
		System.out.println(floor + " floor, and");
		System.out.println(windows + " windows.");
	}

}
